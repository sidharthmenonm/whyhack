import bootstrap from 'bootstrap'
import Popper from 'popper.js';
import TypeIt from "typeit";

window.bootstrap = bootstrap;

function findPos(obj) {
  var curtop = 0;
  if (obj.offsetParent) {
      do {
          curtop += obj.offsetTop;
      } while (obj = obj.offsetParent);
  return [curtop];
  }
}

$('[data-scroll-link]').on('click', function(event){
  event.preventDefault();

  var target = $(this).data('target');
  var offset = $(this).data('offset');

  window.scroll({
    left:0,
    top:findPos(document.getElementById(target))-offset, 
    behavior:'smooth'
  });

});

$('#timeline-section .item').on('click', function() {
  var selected = $(this).data('content');
  $('#timeline-section .item').removeClass('active');
  $(this).addClass('active');

  $("#timeline-section .content").removeClass('active');
  $("#timeline-section #" + selected).addClass('active');
})

$('.apply').on('click', function() {
  $('#timeline-section .item').removeClass('active');
  $('#timeline-section .item.one').addClass('active');

  $("#timeline-section .content").removeClass('active');
  $("#timeline-section #content-1").addClass('active');

  setTimeout(function() {
    window.scrollTo(0, document.querySelector('#timeline-section').offsetTop);
  }, 1000);

})

new TypeIt("#typeit", {
    speed: 100,
    waitUntilVisible: true,
    afterComplete: () => {
      document.querySelector('.partner-header').classList.add('open');
    }
  })
  .type("innovate h", { delay: 300 })
  .delete(3)
  .type('h', { delay: 300 })
  .move('START', { delay: 300 })
  .move(1, { delay: 300 })
  .delete(1)
  .type('I', { delay: 300 })
  .move('END')
  .type('er', { delay: 500 })
  .delete(3, { delay: 300 })
  .type('<i>H</i>', { delay: 300 })
  .type('er')
  .break({ delay: 500 })
  // .type('<span>Moulding her to innovate better!</span>')
  .type('<span>to go beyond the impossible!</span>')
  .go();



var site = site || {};
site.window = $(window);
site.document = $(document);
site.Width = 600
site.Height = 600

var Background = function() {

};

Background.headparticle = function() {

  var camera, scene, renderer;
  var mouseX = 240.5,
    mouseY = 0.5;
  var p;

  var windowHalfX = site.Width / 2;
  var windowHalfY = site.Height / 2;

  Background.camera = new THREE.PerspectiveCamera(35, site.Width / site.Height, 1, 2000);
  Background.camera.position.z = 250; //300

  // scene
  Background.scene = new THREE.Scene();

  // texture
  var manager = new THREE.LoadingManager();
  manager.onProgress = function(item, loaded, total) {
    //console.log('webgl, twice??');
    //console.log( item, loaded, total );
  };


  // particles
  var p_geom = new THREE.Geometry();
  var p_material = new THREE.ParticleBasicMaterial({
    color: 0x00afef,
    size: .75 //1.5
  });

  // model
  var loader = new THREE.OBJLoader(manager);
  loader.load('Female Head.obj', function(object) {

    object.traverse(function(child) {

      if (child instanceof THREE.Mesh) {

        // child.material.map = texture;

        var scale = 70; //8;
        var count = 0;
        $(child.geometry.vertices).each(function() {
          if (count++ % 40 == 0) {
            p_geom.vertices.push(new THREE.Vector3(this.x * scale, this.y * scale, this.z * scale));
          }
        })
      }
    });

    document.querySelector('.particlehead').classList.remove('loading');
    Background.scene.add(p)
  });

  p = new THREE.ParticleSystem(
    p_geom,
    p_material
  );

  p.position.y = 25;

  Background.renderer = new THREE.WebGLRenderer({ alpha: true });
  Background.renderer.setSize(site.Width, site.Height);
  Background.renderer.setClearColor(0x000000, 0);

  $('.particlehead').append(Background.renderer.domElement);
  $('#main-section').on('mousemove', onDocumentMouseMove);
  site.window.on('resize', onWindowResize);

  function onWindowResize() {
    windowHalfX = site.Width / 2;
    windowHalfY = site.Height / 2;
    //console.log(windowHalfX);

    Background.camera.aspect = site.Width / site.Height;
    Background.camera.updateProjectionMatrix();

    Background.renderer.setSize(site.Width, site.Height);
  }

  function onDocumentMouseMove(event) {
    mouseX = (event.clientX - windowHalfX) / 2;
    mouseY = (event.clientY - windowHalfY) / 2;
  }

  Background.animate = function() {

    Background.ticker = TweenMax.ticker;
    Background.ticker.addEventListener("tick", Background.animate);

    render();
  }

  function render() {
    Background.camera.position.x += ((mouseX * .5) - Background.camera.position.x) * .05;
    Background.camera.position.y += (-(mouseY * .5) - Background.camera.position.y) * .05;

    // console.log(mouseX, mouseY)

    Background.camera.lookAt(Background.scene.position);

    Background.renderer.render(Background.scene, Background.camera);
  }

  render();

  Background.animate();
};


Background.headparticle();


(function() {
  try {
    var f = document.createElement("iframe");
    f.src = 'https://forms.zohopublic.com/keralastartupmission/form/WhyHackApplicationform/formperma/7sV85ZUo0BAHnIv8JHF4_nAP_igeIy1ZVOlklStpinM?zf_rszfm=1';
    f.style.border = "none";
    f.style.height = "1600px";
    f.style.width = "90%";
    f.style.transition = "all 0.5s ease"; // No I18N
    var d = document.getElementById("zf_div_7sV85ZUo0BAHnIv8JHF4_nAP_igeIy1ZVOlklStpinM");
    d.appendChild(f);
    window.addEventListener('message', function() {
      var zf_ifrm_data = event.data.split("|");
      console.log(zf_ifrm_data);
      var zf_perma = zf_ifrm_data[0];
      var zf_ifrm_ht_nw = (parseInt(zf_ifrm_data[1], 10) + 55) + "px";
      var iframe = document.getElementById("zf_div_7sV85ZUo0BAHnIv8JHF4_nAP_igeIy1ZVOlklStpinM").getElementsByTagName("iframe")[0];
      if ((iframe.src).indexOf('formperma') > 0 && (iframe.src).indexOf(zf_perma) > 0) {
        var prevIframeHeight = iframe.style.height;
        // if (prevIframeHeight != zf_ifrm_ht_nw) {
        if (zf_ifrm_ht_nw > 1555) {
          iframe.style.height = zf_ifrm_ht_nw;
        }
      }
    }, false);
  } catch (e) {}
})();

(function() {
  try {
    var f = document.createElement("iframe");
    f.src = 'https://forms.zohopublic.com/keralastartupmission/form/WhyHackSubmissionform/formperma/8x10cKDCWAXL0GnxbWtg0gllLeAeP0ocSoYXFgIDDT0?zf_rszfm=1';
    f.style.border = "none";
    f.style.height = "884px";
    f.style.width = "90%";
    f.style.transition = "all 0.5s ease"; // No I18N
    var d = document.getElementById("zf_div_8x10cKDCWAXL0GnxbWtg0gllLeAeP0ocSoYXFgIDDT0");
    d.appendChild(f);
    window.addEventListener('message', function() {
      var zf_ifrm_data = event.data.split("|");
      var zf_perma = zf_ifrm_data[0];
      var zf_ifrm_ht_nw = (parseInt(zf_ifrm_data[1], 10) + 15) + "px";
      var iframe = document.getElementById("zf_div_8x10cKDCWAXL0GnxbWtg0gllLeAeP0ocSoYXFgIDDT0").getElementsByTagName("iframe")[0];
      if ((iframe.src).indexOf('formperma') > 0 && (iframe.src).indexOf(zf_perma) > 0) {
        // var prevIframeHeight = iframe.style.height;
        if (prevIframeHeight != zf_ifrm_ht_nw) {
          iframe.style.height = zf_ifrm_ht_nw;
        }
      }
    }, false);
  } catch (e) {}
})();